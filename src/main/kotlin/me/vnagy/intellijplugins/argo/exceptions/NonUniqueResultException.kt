package me.vnagy.intellijplugins.argo.exceptions

import java.lang.Exception

class NonUniqueResultException(override val message: String?) : Exception() {

}
