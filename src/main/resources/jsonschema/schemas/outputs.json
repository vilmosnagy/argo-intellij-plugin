{
  "type": "object",
  "title": "Outputs hold parameters, artifacts, and results from a step",
  "properties": {
    "artifacts": {
      "type": "array",
      "title": "Artifacts holds the list of output artifacts produced by a step\n+patchStrategy=merge\n+patchMergeKey=name",
      "items": {
        "type": "object",
        "title": "Artifact indicates an artifact to place at a specified path",
        "properties": {
          "archive": {
            "type": "object",
            "title": "ArchiveStrategy describes how to archive files/directory when saving artifacts",
            "properties": {
              "none": {
                "description": "NoneStrategy indicates to skip tar process and upload the files or directory tree as independent\nfiles. Note that if the artifact is a directory, the artifact driver must support the ability to\nsave/load the directory appropriately.",
                "type": "object"
              },
              "tar": {
                "type": "object",
                "title": "TarStrategy will tar and gzip the file or directory when saving",
                "properties": {
                  "compressionLevel": {
                    "description": "CompressionLevel specifies the gzip compression level to use for the artifact.\nDefaults to gzip.DefaultCompression.",
                    "type": "integer",
                    "format": "int32"
                  }
                }
              }
            }
          },
          "artifactLocation": {
            "description": "ArtifactLocation describes a location for a single or multiple artifacts.\nIt is used as single artifact in the context of inputs/outputs (e.g. outputs.artifacts.artname).\nIt is also used to describe the location of multiple artifacts such as the archive location\nof a single workflow step, which the executor will use as a default location to store its files.",
            "type": "object",
            "properties": {
              "archiveLogs": {
                "type": "boolean",
                "format": "boolean",
                "title": "ArchiveLogs indicates if the container logs should be archived"
              },
              "artifactory": {
                "type": "object",
                "title": "ArtifactoryArtifact is the location of an artifactory artifact",
                "properties": {
                  "artifactoryAuth": {
                    "type": "object",
                    "title": "ArtifactoryAuth describes the secret selectors required for authenticating to artifactory",
                    "properties": {
                      "passwordSecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      },
                      "usernameSecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      }
                    }
                  },
                  "url": {
                    "type": "string",
                    "title": "URL of the artifact"
                  }
                }
              },
              "gcs": {
                "type": "object",
                "title": "GCSArtifact is the location of a GCS artifact",
                "properties": {
                  "gCSBucket": {
                    "type": "object",
                    "title": "GCSBucket contains the access information for interfacring with a GCS bucket",
                    "properties": {
                      "bucket": {
                        "type": "string",
                        "title": "Bucket is the name of the bucket"
                      },
                      "serviceAccountKeySecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      }
                    }
                  },
                  "key": {
                    "type": "string",
                    "title": "Key is the path in the bucket where the artifact resides"
                  }
                }
              },
              "git": {
                "type": "object",
                "title": "GitArtifact is the location of an git artifact",
                "properties": {
                  "depth": {
                    "type": "string",
                    "format": "uint64",
                    "title": "Depth specifies clones/fetches should be shallow and include the given\nnumber of commits from the branch tip"
                  },
                  "fetch": {
                    "type": "array",
                    "title": "Fetch specifies a number of refs that should be fetched before checkout",
                    "items": {
                      "type": "string"
                    }
                  },
                  "insecureIgnoreHostKey": {
                    "type": "boolean",
                    "format": "boolean",
                    "title": "InsecureIgnoreHostKey disables SSH strict host key checking during git clone"
                  },
                  "passwordSecret": {
                    "description": "SecretKeySelector selects a key of a Secret.",
                    "type": "object",
                    "properties": {
                      "key": {
                        "description": "The key of the secret to select from.  Must be a valid secret key.",
                        "type": "string"
                      },
                      "localObjectReference": {
                        "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                        "type": "object",
                        "properties": {
                          "name": {
                            "type": "string",
                            "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                          }
                        }
                      },
                      "optional": {
                        "type": "boolean",
                        "format": "boolean",
                        "title": "Specify whether the Secret or its key must be defined\n+optional"
                      }
                    }
                  },
                  "repo": {
                    "type": "string",
                    "title": "Repo is the git repository"
                  },
                  "revision": {
                    "type": "string",
                    "title": "Revision is the git commit, tag, branch to checkout"
                  },
                  "sshPrivateKeySecret": {
                    "description": "SecretKeySelector selects a key of a Secret.",
                    "type": "object",
                    "properties": {
                      "key": {
                        "description": "The key of the secret to select from.  Must be a valid secret key.",
                        "type": "string"
                      },
                      "localObjectReference": {
                        "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                        "type": "object",
                        "properties": {
                          "name": {
                            "type": "string",
                            "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                          }
                        }
                      },
                      "optional": {
                        "type": "boolean",
                        "format": "boolean",
                        "title": "Specify whether the Secret or its key must be defined\n+optional"
                      }
                    }
                  },
                  "usernameSecret": {
                    "description": "SecretKeySelector selects a key of a Secret.",
                    "type": "object",
                    "properties": {
                      "key": {
                        "description": "The key of the secret to select from.  Must be a valid secret key.",
                        "type": "string"
                      },
                      "localObjectReference": {
                        "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                        "type": "object",
                        "properties": {
                          "name": {
                            "type": "string",
                            "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                          }
                        }
                      },
                      "optional": {
                        "type": "boolean",
                        "format": "boolean",
                        "title": "Specify whether the Secret or its key must be defined\n+optional"
                      }
                    }
                  }
                }
              },
              "hdfs": {
                "type": "object",
                "title": "HDFSArtifact is the location of an HDFS artifact",
                "properties": {
                  "force": {
                    "type": "boolean",
                    "format": "boolean",
                    "title": "Force copies a file forcibly even if it exists (default: false)"
                  },
                  "hDFSConfig": {
                    "type": "object",
                    "title": "HDFSConfig is configurations for HDFS",
                    "properties": {
                      "addresses": {
                        "type": "array",
                        "title": "Addresses is accessible addresses of HDFS name nodes",
                        "items": {
                          "type": "string"
                        }
                      },
                      "hDFSKrbConfig": {
                        "type": "object",
                        "title": "HDFSKrbConfig is auth configurations for Kerberos",
                        "properties": {
                          "krbCCacheSecret": {
                            "description": "SecretKeySelector selects a key of a Secret.",
                            "type": "object",
                            "properties": {
                              "key": {
                                "description": "The key of the secret to select from.  Must be a valid secret key.",
                                "type": "string"
                              },
                              "localObjectReference": {
                                "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                                "type": "object",
                                "properties": {
                                  "name": {
                                    "type": "string",
                                    "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                                  }
                                }
                              },
                              "optional": {
                                "type": "boolean",
                                "format": "boolean",
                                "title": "Specify whether the Secret or its key must be defined\n+optional"
                              }
                            }
                          },
                          "krbConfigConfigMap": {
                            "description": "Selects a key from a ConfigMap.",
                            "type": "object",
                            "properties": {
                              "key": {
                                "description": "The key to select.",
                                "type": "string"
                              },
                              "localObjectReference": {
                                "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                                "type": "object",
                                "properties": {
                                  "name": {
                                    "type": "string",
                                    "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                                  }
                                }
                              },
                              "optional": {
                                "type": "boolean",
                                "format": "boolean",
                                "title": "Specify whether the ConfigMap or its key must be defined\n+optional"
                              }
                            }
                          },
                          "krbKeytabSecret": {
                            "description": "SecretKeySelector selects a key of a Secret.",
                            "type": "object",
                            "properties": {
                              "key": {
                                "description": "The key of the secret to select from.  Must be a valid secret key.",
                                "type": "string"
                              },
                              "localObjectReference": {
                                "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                                "type": "object",
                                "properties": {
                                  "name": {
                                    "type": "string",
                                    "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                                  }
                                }
                              },
                              "optional": {
                                "type": "boolean",
                                "format": "boolean",
                                "title": "Specify whether the Secret or its key must be defined\n+optional"
                              }
                            }
                          },
                          "krbRealm": {
                            "description": "KrbRealm is the Kerberos realm used with Kerberos keytab\nIt must be set if keytab is used.",
                            "type": "string"
                          },
                          "krbServicePrincipalName": {
                            "description": "KrbServicePrincipalName is the principal name of Kerberos service\nIt must be set if either ccache or keytab is used.",
                            "type": "string"
                          },
                          "krbUsername": {
                            "description": "KrbUsername is the Kerberos username used with Kerberos keytab\nIt must be set if keytab is used.",
                            "type": "string"
                          }
                        }
                      },
                      "hdfsUser": {
                        "description": "HDFSUser is the user to access HDFS file system.\nIt is ignored if either ccache or keytab is used.",
                        "type": "string"
                      }
                    }
                  },
                  "path": {
                    "type": "string",
                    "title": "Path is a file path in HDFS"
                  }
                }
              },
              "http": {
                "type": "object",
                "title": "HTTPArtifact allows an file served on HTTP to be placed as an input artifact in a container",
                "properties": {
                  "url": {
                    "type": "string",
                    "title": "URL of the artifact"
                  }
                }
              },
              "oss": {
                "type": "object",
                "title": "OSSArtifact is the location of an OSS artifact",
                "properties": {
                  "key": {
                    "type": "string",
                    "title": "Key is the path in the bucket where the artifact resides"
                  },
                  "oSSBucket": {
                    "type": "object",
                    "title": "OSSBucket contains the access information required for interfacing with an OSS bucket",
                    "properties": {
                      "accessKeySecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      },
                      "bucket": {
                        "type": "string",
                        "title": "Bucket is the name of the bucket"
                      },
                      "endpoint": {
                        "type": "string",
                        "title": "Endpoint is the hostname of the bucket endpoint"
                      },
                      "secretKeySecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      }
                    }
                  }
                }
              },
              "raw": {
                "type": "object",
                "title": "RawArtifact allows raw string content to be placed as an artifact in a container",
                "properties": {
                  "data": {
                    "type": "string",
                    "title": "Data is the string contents of the artifact"
                  }
                }
              },
              "s3": {
                "type": "object",
                "title": "S3Artifact is the location of an S3 artifact",
                "properties": {
                  "key": {
                    "type": "string",
                    "title": "Key is the key in the bucket where the artifact resides"
                  },
                  "s3Bucket": {
                    "type": "object",
                    "title": "S3Bucket contains the access information required for interfacing with an S3 bucket",
                    "properties": {
                      "accessKeySecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      },
                      "bucket": {
                        "type": "string",
                        "title": "Bucket is the name of the bucket"
                      },
                      "endpoint": {
                        "type": "string",
                        "title": "Endpoint is the hostname of the bucket endpoint"
                      },
                      "insecure": {
                        "type": "boolean",
                        "format": "boolean",
                        "title": "Insecure will connect to the service with TLS"
                      },
                      "region": {
                        "type": "string",
                        "title": "Region contains the optional bucket region"
                      },
                      "roleARN": {
                        "description": "RoleARN is the Amazon Resource Name (ARN) of the role to assume.",
                        "type": "string"
                      },
                      "secretKeySecret": {
                        "description": "SecretKeySelector selects a key of a Secret.",
                        "type": "object",
                        "properties": {
                          "key": {
                            "description": "The key of the secret to select from.  Must be a valid secret key.",
                            "type": "string"
                          },
                          "localObjectReference": {
                            "description": "LocalObjectReference contains enough information to let you locate the\nreferenced object inside the same namespace.",
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Name of the referent.\nMore info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names\nTODO: Add other useful fields. apiVersion, kind, uid?\n+optional"
                              }
                            }
                          },
                          "optional": {
                            "type": "boolean",
                            "format": "boolean",
                            "title": "Specify whether the Secret or its key must be defined\n+optional"
                          }
                        }
                      },
                      "useSDKCreds": {
                        "description": "UseSDKCreds tells the driver to figure out credentials based on sdk defaults.",
                        "type": "boolean",
                        "format": "boolean"
                      }
                    }
                  }
                }
              }
            }
          },
          "from": {
            "type": "string",
            "title": "From allows an artifact to reference an artifact from a previous step"
          },
          "globalName": {
            "type": "string",
            "title": "GlobalName exports an output artifact to the global scope, making it available as\n'{{io.argoproj.workflow.v1alpha1.outputs.artifacts.XXXX}} and in workflow.status.outputs.artifacts"
          },
          "mode": {
            "description": "mode bits to use on this file, must be a value between 0 and 0777\nset when loading input artifacts.",
            "type": "integer",
            "format": "int32"
          },
          "name": {
            "description": "name of the artifact. must be unique within a template's inputs/outputs.",
            "type": "string"
          },
          "optional": {
            "type": "boolean",
            "format": "boolean",
            "title": "Make Artifacts optional, if Artifacts doesn't generate or exist"
          },
          "path": {
            "type": "string",
            "title": "Path is the container path to the artifact"
          }
        }
      }
    },
    "exitCode": {
      "type": "string",
      "title": "ExitCode holds the exit code of a script template"
    },
    "parameters": {
      "type": "array",
      "title": "Parameters holds the list of output parameters produced by a step\n+patchStrategy=merge\n+patchMergeKey=name",
      "items": {
        "type": "object",
        "title": "Parameter indicate a passed string parameter to a service template with an optional default value",
        "properties": {
          "default": {
            "type": "string",
            "title": "Default is the default value to use for an input parameter if a value was not supplied\nDEPRECATED: This field is not used"
          },
          "globalName": {
            "type": "string",
            "title": "GlobalName exports an output parameter to the global scope, making it available as\n'{{io.argoproj.workflow.v1alpha1.outputs.parameters.XXXX}} and in workflow.status.outputs.parameters"
          },
          "name": {
            "type": "string",
            "title": "Name is the parameter name"
          },
          "value": {
            "type": "string",
            "title": "Value is the literal value to use for the parameter.\nIf specified in the context of an input parameter, the value takes precedence over any passed values"
          },
          "valueFrom": {
            "type": "object",
            "title": "ValueFrom describes a location in which to obtain the value to a parameter",
            "properties": {
              "default": {
                "type": "string",
                "title": "Default specifies a value to be used if retrieving the value from the specified source fails"
              },
              "jqFilter": {
                "type": "string",
                "title": "JQFilter expression against the resource object in resource templates"
              },
              "jsonPath": {
                "type": "string",
                "title": "JSONPath of a resource to retrieve an output parameter value from in resource templates"
              },
              "parameter": {
                "type": "string",
                "title": "Parameter reference to a step or dag task in which to retrieve an output parameter value from\n(e.g. '{{steps.mystep.outputs.myparam}}')"
              },
              "path": {
                "type": "string",
                "title": "Path in the container to retrieve an output parameter value from in container templates"
              }
            }
          }
        }
      }
    },
    "result": {
      "type": "string",
      "title": "Result holds the result (stdout) of a script template"
    }
  },
  "$schema": "http://json-schema.org/schema#"
}